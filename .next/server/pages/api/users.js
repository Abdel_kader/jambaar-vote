"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/users";
exports.ids = ["pages/api/users"];
exports.modules = {

/***/ "@prisma/client":
/*!*********************************!*\
  !*** external "@prisma/client" ***!
  \*********************************/
/***/ ((module) => {

module.exports = require("@prisma/client");

/***/ }),

/***/ "(api)/./pages/api/users/index.tsx":
/*!***********************************!*\
  !*** ./pages/api/users/index.tsx ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* export default binding */ __WEBPACK_DEFAULT_EXPORT__)\n/* harmony export */ });\n/* harmony import */ var _prisma_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @prisma/client */ \"@prisma/client\");\n/* harmony import */ var _prisma_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_prisma_client__WEBPACK_IMPORTED_MODULE_0__);\n\n/* harmony default export */ async function __WEBPACK_DEFAULT_EXPORT__(req, res) {\n    const prisma = new _prisma_client__WEBPACK_IMPORTED_MODULE_0__.PrismaClient({\n        log: [\n            'query'\n        ]\n    });\n    const type = req.query.type;\n    const date = new Date().toISOString().substr(0, 10).replace('T', ' ');\n    try {\n        if (typeof type == \"string\") {\n            const users = await prisma.user.findMany({\n                where: {\n                    type: parseInt(type),\n                    dateSeance: date\n                }\n            });\n            console.log(users.length);\n            if (users.length > 0) {\n                res.json(users);\n            } else {\n                res.status(204);\n                res.json({\n                    message: 'Aucun enregistrement trouv\\xe9 !'\n                });\n            }\n        }\n    } catch (e) {\n        res.status(500);\n        res.json({\n            error: e.message\n        });\n    } finally{\n        await prisma.$disconnect();\n    }\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9wYWdlcy9hcGkvdXNlcnMvaW5kZXgudHN4LmpzIiwibWFwcGluZ3MiOiI7Ozs7OztBQUM2QztBQUU3Qyw2QkFBZSwwQ0FBZ0JDLEdBQW1CLEVBQUVDLEdBQW9CLEVBQUUsQ0FBQztJQUN2RSxLQUFLLENBQUNDLE1BQU0sR0FBRyxHQUFHLENBQUNILHdEQUFZLENBQUMsQ0FBQ0k7UUFBQUEsR0FBRyxFQUFFLENBQUM7WUFBQSxDQUFPO1FBQUEsQ0FBQztJQUFBLENBQUM7SUFDNUMsS0FBSyxDQUFDQyxJQUFJLEdBQUdKLEdBQUcsQ0FBQ0ssS0FBSyxDQUFDRCxJQUFJO0lBQy9CLEtBQUssQ0FBQ0UsSUFBSSxHQUFHLEdBQUcsQ0FBQ0MsSUFBSSxHQUFHQyxXQUFXLEdBQUdDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsRUFBRSxFQUFFQyxPQUFPLENBQUMsQ0FBRyxJQUFFLENBQUc7SUFDcEUsR0FBRyxDQUFDLENBQUM7UUFFRCxFQUFFLEVBQUUsTUFBTSxDQUFDTixJQUFJLElBQUksQ0FBUSxTQUFFLENBQUM7WUFDMUIsS0FBSyxDQUFDTyxLQUFLLEdBQUcsS0FBSyxDQUFDVCxNQUFNLENBQUNVLElBQUksQ0FBQ0MsUUFBUSxDQUFDLENBQUM7Z0JBQ3RDQyxLQUFLLEVBQUUsQ0FBQztvQkFDSlYsSUFBSSxFQUFFVyxRQUFRLENBQUNYLElBQUk7b0JBQ25CWSxVQUFVLEVBQUVWLElBQUk7Z0JBQ3BCLENBQUM7WUFDTCxDQUFDO1lBQ0RXLE9BQU8sQ0FBQ2QsR0FBRyxDQUFDUSxLQUFLLENBQUNPLE1BQU07WUFDeEIsRUFBRSxFQUFDUCxLQUFLLENBQUNPLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQztnQkFDbEJqQixHQUFHLENBQUNrQixJQUFJLENBQUNSLEtBQUs7WUFDbEIsQ0FBQyxNQUFNLENBQUM7Z0JBQ0pWLEdBQUcsQ0FBQ21CLE1BQU0sQ0FBQyxHQUFHO2dCQUNkbkIsR0FBRyxDQUFDa0IsSUFBSSxDQUFDLENBQUNFO29CQUFBQSxPQUFPLEVBQUUsQ0FBK0I7Z0JBQUEsQ0FBQztZQUN2RCxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUMsQ0FBQyxLQUFLLEVBQUVDLENBQUMsRUFBRSxDQUFDO1FBQ1RyQixHQUFHLENBQUNtQixNQUFNLENBQUMsR0FBRztRQUNkbkIsR0FBRyxDQUFDa0IsSUFBSSxDQUFDLENBQUNJO1lBQUFBLEtBQUssRUFBRUQsQ0FBQyxDQUFDRCxPQUFPO1FBQUEsQ0FBQztJQUMvQixDQUFDLFFBQVMsQ0FBQztRQUNQLEtBQUssQ0FBQ25CLE1BQU0sQ0FBQ3NCLFdBQVc7SUFDNUIsQ0FBQztBQUVMLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly93aXRoLXRhaWx3aW5kY3NzLy4vcGFnZXMvYXBpL3VzZXJzL2luZGV4LnRzeD9hNjllIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5leHRBcGlSZXF1ZXN0LCBOZXh0QXBpUmVzcG9uc2UgfSBmcm9tIFwibmV4dFwiO1xyXG5pbXBvcnQgeyBQcmlzbWFDbGllbnQgfSBmcm9tIFwiQHByaXNtYS9jbGllbnRcIlxyXG5cclxuZXhwb3J0IGRlZmF1bHQgYXN5bmMgZnVuY3Rpb24gKHJlcTogTmV4dEFwaVJlcXVlc3QsIHJlczogTmV4dEFwaVJlc3BvbnNlKSB7XHJcbiAgICBjb25zdCBwcmlzbWEgPSBuZXcgUHJpc21hQ2xpZW50KHtsb2c6IFsncXVlcnknXX0pO1xyXG4gICAgICAgIGNvbnN0IHR5cGUgPSByZXEucXVlcnkudHlwZTtcclxuICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSgpLnRvSVNPU3RyaW5nKCkuc3Vic3RyKDAsIDEwKS5yZXBsYWNlKCdUJywgJyAnKTtcclxuICAgIHRyeSB7XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgdHlwZSA9PSBcInN0cmluZ1wiKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHVzZXJzID0gYXdhaXQgcHJpc21hLnVzZXIuZmluZE1hbnkoe1xyXG4gICAgICAgICAgICAgICAgd2hlcmU6IHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiBwYXJzZUludCh0eXBlKSxcclxuICAgICAgICAgICAgICAgICAgICBkYXRlU2VhbmNlOiBkYXRlXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh1c2Vycy5sZW5ndGgpXHJcbiAgICAgICAgICAgIGlmKHVzZXJzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHJlcy5qc29uKHVzZXJzKVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmVzLnN0YXR1cygyMDQpXHJcbiAgICAgICAgICAgICAgICByZXMuanNvbih7bWVzc2FnZTogJ0F1Y3VuIGVucmVnaXN0cmVtZW50IHRyb3V2w6kgISd9KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSBjYXRjaCAoZSkge1xyXG4gICAgICAgIHJlcy5zdGF0dXMoNTAwKTtcclxuICAgICAgICByZXMuanNvbih7ZXJyb3I6IGUubWVzc2FnZX0pO1xyXG4gICAgfSBmaW5hbGx5IHtcclxuICAgICAgICBhd2FpdCBwcmlzbWEuJGRpc2Nvbm5lY3QoKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl0sIm5hbWVzIjpbIlByaXNtYUNsaWVudCIsInJlcSIsInJlcyIsInByaXNtYSIsImxvZyIsInR5cGUiLCJxdWVyeSIsImRhdGUiLCJEYXRlIiwidG9JU09TdHJpbmciLCJzdWJzdHIiLCJyZXBsYWNlIiwidXNlcnMiLCJ1c2VyIiwiZmluZE1hbnkiLCJ3aGVyZSIsInBhcnNlSW50IiwiZGF0ZVNlYW5jZSIsImNvbnNvbGUiLCJsZW5ndGgiLCJqc29uIiwic3RhdHVzIiwibWVzc2FnZSIsImUiLCJlcnJvciIsIiRkaXNjb25uZWN0Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///(api)/./pages/api/users/index.tsx\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("(api)/./pages/api/users/index.tsx"));
module.exports = __webpack_exports__;

})();